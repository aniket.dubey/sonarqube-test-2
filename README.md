# FlytBase

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.7.

## FlytBase frontend dev server

1. Run `npx nx serve operations` for a Frontend dev server.
1. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Integrate with Production/Staging backend without deploying a local backend

1. Run `npx nx run operations:serve:staging` or `npx nx operations:serve:production` for a frontend server to communicate with backend running in staging or production server.
1. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Backend dev server

Run `docker compose up --build -d`. If you want to add your own service to the docker-compose file, please follow steps 1-4 of section 'How to integrate a new Nest Micro service?' detailed in this [Readme file](/gitlab-pipelines/README.md).

## Backend server with web-dev vm

Run `docker compose -f docker-compose.dev-local.yml up --build -d`. If you want to add your own service to the docker-compose file, please follow steps 1-4 of section 'How to integrate a new Nest Micro service?' detailed in this [Readme file](/gitlab-pipelines/README.md).
