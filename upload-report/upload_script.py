import gspread
from oauth2client.service_account import ServiceAccountCredentials
import json
from datetime import datetime, timezone, timedelta

# Load the JSON data
# with open('r.json', 'r') as json_file:
with open('../sonar-report/r.json', 'r') as json_file:
    data = json.load(json_file)

# Set up Google Sheets credentials
scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('your_google_sheets_credentials.json', scope)

gc = gspread.authorize(credentials)

# sh = gc.open(spreadsheet_title)
# sh = gc.open_by_key('1GwAK7tp7C7g6rTstdWfe1aFM7Rs5EGhxqCG3eGiDxRM') # my
sh = gc.open_by_key('1qv8gRTrHvu2fYjWfzcIf5piXOnd54SYeKag0aa9AUUM')
worksheet_title = 'sonar'
worksheet = sh.worksheet(worksheet_title)

# Helper function to format date in Indian timezone
def format_date(date_str):
    date_object = datetime.strptime(date_str, '%A, %b %d, %Y')
    indian_timezone = timezone(timedelta(hours=5, minutes=30))
    date_object = date_object.replace(tzinfo=indian_timezone)
    formatted_date = date_object.strftime('%d/%m/%Y')
    return formatted_date

# Read the entire worksheet into memory
existing_data = worksheet.get_all_values()

# Create a dictionary to map key to row index
rule_key_to_row = {row[13]: idx + 1 for idx, row in enumerate(existing_data)}

# Helper function to check if other values match for an existing issue
def issue_matches_existing(issue, existing_row, existing_data):
    existing_values = existing_data[existing_row - 1]
    existing_rule, existing_severity, existing_status, existing_component, existing_line, existing_textRange, existing_description, existing_message = existing_values[1:9]

    print(f"Reopening issue {existing_rule} {existing_line}.")
    return (
        issue['rule'] == existing_rule and
        issue['severity'] == existing_severity and
        issue['component'] == existing_component and
        str(issue['line']) == existing_line and
        issue['textRange'] == existing_textRange and
        issue['description'] == existing_description and
        issue['message'] == existing_message
    )

# Iterate through the issues in the JSON data
for issue in data['issues']:
    issue_type = issue['type']
    rule_key = issue['rule']
    severity = issue['severity']
    component = issue['component']

    if 'line' not in issue:
        print("Error: 'line' key not found in the issue dictionary.")
        continue  # Skip this issue and proceed to the next one
        

    line = issue['line']
    textRange = issue['textRange']
    message = issue['message']
    status = issue['status']
    key = str(issue_type) + str(rule_key) + str(severity) + str(component) + str(line) + str(message)
    a = key


    # Check if the issue is already present in the sheet
    if key in rule_key_to_row:
        existing_row = rule_key_to_row[key]
        # Check if the row index is within the bounds of existing_data
        if 1 <= existing_row <= len(existing_data):
            existing_status = existing_data[existing_row - 1][3]
            print(f"Rule: {rule_key}, line: {line}, Existing Status: {existing_status}, JSON Status: {status}")
            if existing_status.lower() == 'closed' and issue_matches_existing(issue, existing_row, existing_data):
                # Reopen the closed issue by updating status in existing_data
                if status.lower() == 'open' or status.lower() == 'to_review':
                    existing_data[existing_row - 1][3] = status
                    existing_data[existing_row - 1][1] = f'=HYPERLINK("{data["sonarBaseURL"]}/coding_rules#rule_key={rule_key}", "{rule_key}")'
                    # existing_data[existing_row - 1][1] = rule_key

            elif existing_status.lower() == 'false_positive' and issue_matches_existing(issue, existing_row, existing_data):
                # Skip if the rule is already in 'FALSE_POSITIVE' status
                existing_data[existing_row - 1][1] = f'=HYPERLINK("{data["sonarBaseURL"]}/coding_rules#rule_key={rule_key}", "{rule_key}")'
                print(f"Issue {rule_key} is already in 'FALSE_POSITIVE' status.")

            elif status.lower() in ('open', 'to_review'):
                # Skip if the rule is already in 'OPEN' or 'TO_REVIEW' status
                existing_data[existing_row - 1][1] = f'=HYPERLINK("{data["sonarBaseURL"]}/coding_rules#rule_key={rule_key}", "{rule_key}")'
                # existing_data[existing_row - 1][1] = rule_key
                print(f"Issue {rule_key} is already in 'OPEN' or 'TO_REVIEW' status.")
            else:
                # Update the status for other cases
                existing_data[existing_row - 1][3] = status
        else:
            print(f"Row index {existing_row} out of bounds.")
    else:
        # If the rule_key is not present, append a new row
        existing_data.append([
            issue['type'],
            f'=HYPERLINK("{data["sonarBaseURL"]}/coding_rules#rule_key={rule_key}", "{rule_key}")',
            # issue['rule'],
            issue['severity'], status, issue['component'],
            issue['line'], issue['textRange'], issue['description'], issue['message'], '', '', format_date(data['date']), issue['key'], key
        ])

# Update the entire worksheet with the modified existing_data
rng = f"'{worksheet_title}'!A1"
sh.values_update(rng, params={'valueInputOption': 'USER_ENTERED'}, body={'values': existing_data})

print("Script completed.")